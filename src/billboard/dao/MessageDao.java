package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import billboard.beans.Message;
import billboard.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // subject
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date

            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUser_Id());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void MessageDelete(Connection connection, Message message )throws SQLException {
		 PreparedStatement ps = null;

		  try {
			  String sql = "delete FROM messages WHERE id = ?";



	            ps = connection.prepareStatement(sql.toString()); //DBに入れる前の準備（String型にしている）

	            ps.setInt(1, message.getId());

	            ps.executeUpdate(); //データベースへ登録
	        } catch (SQLException e) {
	            throw e;
	        } finally {
	            close(ps);
	        }



	 }


}