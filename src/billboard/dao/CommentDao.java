package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import billboard.beans.Comment;
import billboard.exception.SQLRuntimeException;

public class CommentDao {

	 public void insert(Connection connection, Comment comment) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO comments ( ");
	            sql.append("user_id");
	            sql.append(", text");
	            sql.append(", message_id");
	            sql.append(", created_date");
	            sql.append(", updated_date");
	            sql.append(") VALUES (");
	            sql.append("?");   // user_id
	            sql.append(", ?"); // text
	            sql.append(", ?"); // message_id
	            sql.append(", CURRENT_TIMESTAMP"); // created_date
	            sql.append(", CURRENT_TIMESTAMP"); // updated_date
	            sql.append(")");

	            ps = connection.prepareStatement(sql.toString()); //DBに入れる前の準備（String型にしている）

	            ps.setInt(1, comment.getUser_Id()); //「1」は「?」の置き換え
	            ps.setString(2, comment.getText());
	            ps.setInt(3, comment.getMessage_id());

	            ps.executeUpdate(); //データベースへ登録
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }
	 public void CommentDelete(Connection connection, Comment comment)throws SQLException {
		 PreparedStatement ps = null;

		  try {
			  String sql = "delete FROM comments WHERE id = ?";



	            ps = connection.prepareStatement(sql.toString()); //DBに入れる前の準備（String型にしている）

	            ps.setInt(1, comment.getId());

	            ps.executeUpdate(); //データベースへ登録
	        } catch (SQLException e) {
	            throw e;
	        } finally {
	            close(ps);
	        }
	 }
}
