package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import billboard.beans.Position;
import billboard.exception.SQLRuntimeException;

	public class PositionDao {
		public void insert(Connection connection, Position position) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO positions ( ");
	            sql.append("name");
	            sql.append(", created_date");
	            sql.append(", updated_date");
	            sql.append(") VALUES (");
	            sql.append(" ?"); // name
	            sql.append(", CURRENT_TIMESTAMP"); // created_date
	            sql.append(", CURRENT_TIMESTAMP"); // updated_date
	            sql.append(")");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, position.getName());

	            ps.executeUpdate();

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }


		//データの抽出
		public List<Position> getPosition( Connection connection ) {

			 PreparedStatement ps = null;
		        try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("SELECT ");
		            sql.append("positions.id as id, ");
		            sql.append("positions.name as name ");
		            sql.append("FROM positions ");

		            ps = connection.prepareStatement(sql.toString());

		            ResultSet rs = ps.executeQuery();
		            List<Position> ret = toPositionList(rs);
		            return ret;
		        } catch (SQLException e) {
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(ps);
		        }
		}
		private List<Position> toPositionList(ResultSet rs)
	            throws SQLException {

	        List<Position> ret = new ArrayList<Position>();
	        try {
	            while (rs.next()) {
	                String name = rs.getString("name");
	                int id = rs.getInt("id");

	                Position message = new Position();
	                message.setName(name);
	                message.setId(id);

	                ret.add(message);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }

}
