package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import billboard.beans.UserMessage;
import billboard.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num , String timeStart ,String timeEnd,String cateName) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.category as category, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("where (messages.created_date between ? and ?) ");   //投稿時刻の範囲を取得
            if(cateName != null) {

            sql.append("AND messages.category LIKE ? ");                    //カテゴリーのパターンを取得


            }
            sql.append("ORDER BY created_date DESC limit " + num);


            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, timeStart); //surviceから来る
            ps.setString(2, timeEnd);   //surviceから来る
            if(cateName != null) {
            ps.setString(3, "%" + cateName + "%");   //取得した情報を確定化
            }
            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String category = rs.getString("category");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setLogin_id(login_id);
                message.setName(name);
                message.setId(id);
                message.setUser_id(user_id);
                message.setCategory(category);
                message.setSubject(subject);
                message.setText(text);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}