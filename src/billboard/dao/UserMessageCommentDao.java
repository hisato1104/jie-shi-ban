package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import billboard.beans.UserMessageComment;
import billboard.exception.SQLRuntimeException;

public class UserMessageCommentDao {

	public List<UserMessageComment> getUserMessagesComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date ASC limit " + num);


            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<UserMessageComment> ret = toUserMessageCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessageComment> toUserMessageCommentList(ResultSet rs)
            throws SQLException {

        List<UserMessageComment> ret = new ArrayList<UserMessageComment>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
//                String login_id = rs.getString("login_id");
                String text = rs.getString("text");
                int message_id = rs.getInt("message_id");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");
               // Timestamp updatedDate = rs.getTimestamp("updated_date");

                UserMessageComment message = new UserMessageComment();
                message.setId(id);
                message.setUser_id(user_id);
//                message.setLogin_id(login_id);
                message.setText(text);
                message.setMessage_id(message_id);
                message.setName(name);
                message.setCreated_date(createdDate);
                //message.setUpdated_date(updatedDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}

