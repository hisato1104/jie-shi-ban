package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import billboard.beans.UserManagement;
import billboard.exception.SQLRuntimeException;

	public class UserManagementDao {

		public List<UserManagement> getUserManagement(Connection connection, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();

	            sql.append("SELECT ");
	            sql.append("users.id as id, ");
	            sql.append("users.name as name, ");
	            sql.append("users.login_id as login_id, ");
	            sql.append("branches.name as branch_name, ");
	            sql.append("positions.name as position_name, ");
	            sql.append("is_stopped as is_stopped ");
	            sql.append("FROM users INNER JOIN branches ");
	            sql.append("ON users.branch_id = branches.id ");
	            sql.append("INNER JOIN positions ");
	            sql.append("ON users.position_id = positions.id ");
	            sql.append("ORDER BY id DESC ");



	            ps = connection.prepareStatement(sql.toString());

	            ResultSet rs = ps.executeQuery();

	            List<UserManagement> ret = toUserManagementList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    private List<UserManagement> toUserManagementList(ResultSet rs)
	            throws SQLException {

	        List<UserManagement> ret = new ArrayList<UserManagement>();
	        try {
	            while (rs.next()) {
	            	int id = rs.getInt("id");
	            	String name = rs.getString("name");
	            	String login_id = rs.getString("login_id");
	            	String branch_name = rs.getString("branch_name");
	            	String position_name = rs.getString("position_name");
	            	int is_stopped = rs.getInt("is_stopped");


	                UserManagement message = new UserManagement();
	                message.setId(id);
	                message.setName(name);
	                message.setLogin_id(login_id);
	                message.setBranch_name(branch_name);
	                message.setPosition_name(position_name);
	                message.setIs_stopped(is_stopped);


	                ret.add(message);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }
}
