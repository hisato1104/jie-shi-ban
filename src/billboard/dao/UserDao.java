package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import billboard.beans.User;
import billboard.exception.NoRowsUpdatedRuntimeException;
import billboard.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); // is_stopped
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString()); //DBに入れる前の準備（String型にしている）

            ps.setString(1, user.getLogin_id()); //「1」は「?」の置き換え
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            ps.setInt(6, user.getIs_stopped());

            ps.executeUpdate(); //データベースへ登録
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String login_id,
            String password, String is_stopped) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_stopped = ?" ;

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, password);
            ps.setString(3, is_stopped);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (3 <= userList.size()) {
                throw new IllegalStateException("3 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUserLoginid(Connection connection, String login_id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? ";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int is_stopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_stopped(is_stopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


//ユーザ編集画面について

    public User getUserId(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // ユーザのアップデート

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", is_stopped = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            ps.setInt(6, user.getIs_stopped());
            ps.setInt(7, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }


    public void UserAlive(Connection connection, User user ){


		 PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("UPDATE users SET");
	            sql.append(" is_stopped = ?");
	            sql.append(" WHERE");
	            sql.append(" id = ?");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setInt(1, user.getIs_stopped());
	            ps.setInt(2, user.getId());

	            ps.executeUpdate();
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }

    	}
}


