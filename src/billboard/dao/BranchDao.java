package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import billboard.beans.Branch;
import billboard.exception.SQLRuntimeException;

public class BranchDao {
	public void insert(Connection connection, Branch branch) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO branches ( ");
            sql.append("name");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // name
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, branch.getName());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
//データの抽出
	public List<Branch> getBranch( Connection connection ) {

		 PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("branches.id as id, ");
	            sql.append("branches.name as name ");
	            sql.append("FROM branches ");

	            ps = connection.prepareStatement(sql.toString());

	            ResultSet rs = ps.executeQuery();
	            List<Branch> ret = toBranchList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	}
	private List<Branch> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");

                Branch message = new Branch();
                message.setName(name);
                message.setId(id);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
