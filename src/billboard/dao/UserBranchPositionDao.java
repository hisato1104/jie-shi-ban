package billboard.dao;

import static billboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import billboard.beans.User;
import billboard.beans.UserBranchPosition;
import billboard.exception.NoRowsUpdatedRuntimeException;
import billboard.exception.SQLRuntimeException;

public class UserBranchPositionDao {

//ユーザ情報の表示(編集画面にて)
	public UserBranchPosition getUserBranchPosition( Connection connection, int id ) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.id as id, ");
	            sql.append("users.name as name, ");
	            sql.append("users.login_id as login_id, ");
	            sql.append("users.password as password, ");
	            sql.append("users.branch_id as branch_id, ");
	            sql.append("users.position_id as position_id, ");
	            sql.append("branches.name as branch_name, ");
	            sql.append("positions.name as position_name, ");
	            sql.append("branches.created_date as created_date ");
	            sql.append("FROM users ");
	            sql.append("INNER JOIN branches ");
	            sql.append("ON users.branch_id = branches.id ");
	            sql.append("INNER JOIN positions ");
	            sql.append("ON users.position_id = positions.id ");
	            sql.append("WHERE users.id = ? ");

	            ps = connection.prepareStatement(sql.toString());
	            ps.setInt(1, id);

	            ResultSet rs = ps.executeQuery();
	            List<UserBranchPosition> ret = toUserBranchPositionList(rs);
	            return ret.get(0);

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }


	    private List<UserBranchPosition> toUserBranchPositionList(ResultSet rs)
	            throws SQLException {

	        List<UserBranchPosition> ret = new ArrayList<UserBranchPosition>();
	        try {
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String login_id = rs.getString("login_id");
	                String name = rs.getString("name");
	                String password = rs.getString("password");
	                int branch_id = rs.getInt("branch_id");
	                int position_id = rs.getInt("branch_id");
	                String branch_name = rs.getString("branch_name");
	                String position_name = rs.getString("position_name");
	                Timestamp createdDate = rs.getTimestamp("created_date");

	                UserBranchPosition message = new UserBranchPosition();

	                message.setId(id);
	                message.setLogin_id(login_id);
	                message.setName(name);
	                message.setPassword(password);
	                message.setBranch_id(branch_id);
	                message.setPosition_id(position_id);
	                message.setBranch_name(branch_name);
	                message.setPosition_name(position_name);
	                message.setCreated_date(createdDate);

	                ret.add(message);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }


//ユーザ情報の変更(アップデート)
	    public void update(Connection connection, User user) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("UPDATE users SET");
	            sql.append("  login_id = ?");
	            sql.append(", password = ?");
	            sql.append(", name = ?");
	            sql.append(", branch_id = ?");
	            sql.append(", position_id = ?");
	            sql.append(", updated_date = CURRENT_TIMESTAMP");
	            sql.append(" WHERE");
	            sql.append(" id = ?");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, user.getLogin_id());
	            ps.setString(2, user.getPassword());
	            ps.setString(3, user.getName());
	            ps.setInt(4, user.getBranch_id());
	            ps.setInt(5, user.getPosition_id());
	            ps.setInt(6, user.getId());

	            int count = ps.executeUpdate();
	            if (count == 0) {
	                throw new NoRowsUpdatedRuntimeException();
	            }
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }

	    }


	}


