package billboard.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import billboard.beans.User;



@WebFilter(urlPatterns = {"/user_view","/settings"})  //
public class Authorityfilter implements Filter {


    /**
     * Default constructor.
     */
    public Authorityfilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {

		 //ユーザのセッション確認
        User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
        List<String> messages = new ArrayList<String>();
        HttpSession session = ((HttpServletRequest) request).getSession();


        if( user != null ){
            // ユーザーが権限者であれば、ユーザ一覧の確認が可能

        	if(String.valueOf(user.getBranch_id()).equals("1") && String.valueOf(user.getPosition_id()).equals("1")){
        		chain.doFilter(request, response);
        	}else{
        		// ユーザーが権限者でなければ、ユーザ一覧が見れない。
        		((HttpServletResponse)response).sendRedirect("./");
        		messages.add("権限がありません");
                session.setAttribute("errorMessages", messages);

        	}
        }else{

            RequestDispatcher dispatcher = request.getRequestDispatcher("/login");

            dispatcher.forward(request,response);
        }
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

		// TODO Auto-generated method stub

	}

}

