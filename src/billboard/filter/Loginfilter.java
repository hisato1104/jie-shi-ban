package billboard.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import billboard.beans.User;

/**
 * Servlet Filter implementation class Loginfilter
 */
@WebFilter("/*")
public class Loginfilter implements Filter {


    /**
     * Default constructor.
     */
    public Loginfilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {

		 // セッションが存在しない場合NULLを返す
        HttpSession session = ((HttpServletRequest)request).getSession(false);
        User loginUser = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");

        if(loginUser == null && !((HttpServletRequest)request).getServletPath().equals("/login") && !((HttpServletRequest)request).getServletPath().equals("/css/style.css") && !((HttpServletRequest)request).getServletPath().equals("/login.jsp") ){
        	List<String> messages = new ArrayList<String>();
        	messages.add("不正アクセスです。");
        	session.setAttribute("errorMessages", messages);
       		((HttpServletResponse) response).sendRedirect("login");
        } else {
        	chain.doFilter(request, response);
        }
        }

//        if(user != null && )
//
//        {
//            // セッションとユーザーがNULLでなければ、通常どおりの遷移
//            chain.doFilter(request, response);
//        }else{
//            // セッションと0ユーザーがNullならば、ログイン画面へ飛ばす
//            RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
//            dispatcher.forward(request,response);
//        }
//	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

		// TODO Auto-generated method stub

	}

}
