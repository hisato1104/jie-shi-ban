package billboard.Service;

import static billboard.utils.CloseableUtil.*;
import static billboard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import billboard.beans.UserManagement;
import billboard.dao.UserManagementDao;


public class UserManagementService {
	private static final int LIMIT_NUM = 500;
    public List<UserManagement> getUserManagement() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserManagementDao usermanagementDao = new UserManagementDao();
	        List<UserManagement> ret = usermanagementDao.getUserManagement(connection, LIMIT_NUM);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
    }

}
