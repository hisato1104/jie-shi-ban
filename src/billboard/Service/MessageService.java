package billboard.Service;

import static billboard.utils.CloseableUtil.*;
import static billboard.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import billboard.beans.Message;
import billboard.beans.UserMessage;
import billboard.dao.MessageDao;
import billboard.dao.UserMessageDao;



public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    private static final int LIMIT_NUM = 1000;
    public List<UserMessage> getMessage(String start,String end,String catename) {

	    Connection connection = null;
	    //デフォルトの設定(投稿日付)
	    String timeStart = "2019-01-01 00:00:00";
	    Date d = new Date(); //現在の日付の取得
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //日付のファーマットを確定
	    String timeEnd = (sdf.format(d));


	    try {
	        connection = getConnection();

	        if (start != null) {
	        	if(start != "") {
	        	timeStart= (start + " 00:00:00");
	        	}
	        }
	        if (end != null) {
	        	if(end != "") {
	        	timeEnd = (end + " 23:59:59");
	        	}
	        	}
	        UserMessageDao messageDao = new UserMessageDao();
	        List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM , timeStart, timeEnd, catename);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }



    }
    public void MessageDelete(Message message)throws SQLException{
    	Connection connection = null;
	    try {
	        connection = getConnection();
	        MessageDao messagedao = new MessageDao();

	        messagedao.MessageDelete(connection, message);

	         commit(connection);

	    }catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }

}

}