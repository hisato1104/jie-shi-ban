package billboard.Service;

import static billboard.utils.CloseableUtil.*;
import static billboard.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import billboard.beans.Comment;
import billboard.beans.UserMessageComment;
import billboard.dao.CommentDao;
import billboard.dao.UserMessageCommentDao;

public class CommentService {
	public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    private static final int LIMIT_NUM = 500;
    public List<UserMessageComment> getComment() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserMessageCommentDao commentDao = new UserMessageCommentDao();
	        List<UserMessageComment> ret = commentDao.getUserMessagesComment(connection, LIMIT_NUM);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
    }
    public void commentDelete(Comment comment)throws SQLException{
    	Connection connection = null;
	    try {
	        connection = getConnection();
	        CommentDao commentdao = new CommentDao();

	         commentdao.CommentDelete(connection, comment);

	         commit(connection);

	    }catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }

}
}
