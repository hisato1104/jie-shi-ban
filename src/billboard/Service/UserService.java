package billboard.Service;

import static billboard.utils.CloseableUtil.*;
import static billboard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import billboard.beans.Branch;
import billboard.beans.Position;
import billboard.beans.User;
import billboard.beans.UserBranchPosition;
import billboard.dao.BranchDao;
import billboard.dao.PositionDao;
import billboard.dao.UserBranchPositionDao;
import billboard.dao.UserDao;
import billboard.utils.CipherUtil;


public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

// branchesの情報取得

    public List<Branch> getBranch() {

    	Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            List<Branch> users = branchDao.getBranch(connection);

            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

 // positionsの情報取得

    public List<Position> getPosition() {

    	Connection connection = null;
        try {
            connection = getConnection();

            PositionDao positionDao = new PositionDao();
            List<Position> users = positionDao.getPosition(connection);

            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    // usersの情報取得

    public User getLoginid(String loginid) {

    	Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User users = userDao.getUserLoginid(connection,loginid);

            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


//ユーザ編集画面

    public UserBranchPosition getUserBranchPosition(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserBranchPositionDao userBranchPositionDao = new UserBranchPositionDao();
            UserBranchPosition users = userBranchPositionDao.getUserBranchPosition(connection,id);

            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    // 変更ユーザの取り込み

    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserBranchPositionDao userDao = new UserBranchPositionDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

//ユーザ検索

    public User getUserId(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User users = userDao.getUserId(connection,id);

            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public void userAlive(User user){
    	Connection connection = null;
	    try {
	        connection = getConnection();
	        UserDao userdao = new UserDao();

	        userdao.UserAlive(connection, user);

	         commit(connection);

	    }catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }

}

}