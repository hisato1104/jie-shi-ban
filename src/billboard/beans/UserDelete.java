package billboard.beans;

import java.io.Serializable;

public class UserDelete implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int is_stopped;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIs_stopped() {
		return is_stopped;
	}
	public void setIs_stopped(int is_stopped) {
		this.is_stopped = is_stopped;
	}


}
