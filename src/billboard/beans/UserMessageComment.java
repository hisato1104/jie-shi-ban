package billboard.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessageComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id; 				//comemnts
    private int user_id; 			//comments(commentsとusers(id)をマッチング)
    private String login_id;		//users
    private String text; 			//comments
    private int message_id; 		//comments(commentsとmessages(id)をマッチング)
    private String name;
    private Date created_date;		//comments
    private Date updated_date;		//comments

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getMessage_id() {
		return message_id;
	}
	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}