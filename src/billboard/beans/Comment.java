package billboard.beans;

import java.io.Serializable;
import java.util.Date;

	public class Comment implements Serializable {
	    private static final long serialVersionUID = 1L;

	    private int id;
	    private int user_Id;
	    private String text;
	    private int message_id;
	    private Date createdDate;
	    private Date updatedDate;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getUser_Id() {
			return user_Id;
		}
		public void setUser_Id(int user_Id) {
			this.user_Id = user_Id;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public int getMessage_id() {
			return message_id;
		}
		public void setMessage_id(int message_id) {
			this.message_id = message_id;
		}
		public Date getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		public Date getUpdatedDate() {
			return updatedDate;
		}
		public void setUpdatedDate(Date updatedDate) {
			this.updatedDate = updatedDate;
		}


	}



