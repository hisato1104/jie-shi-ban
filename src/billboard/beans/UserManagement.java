package billboard.beans;

import java.io.Serializable;

	public class UserManagement implements Serializable {

	    private static final long serialVersionUID = 1L;

	    private int id; 					//users
	    private String name; 				//users
	    private String login_id; 			//users
	    private String branch_name;		//branches
	    private String position_name; 		//positions
	    private int is_stopped;			//users

	    public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getLogin_id() {
			return login_id;
		}
		public void setLogin_id(String login_id) {
			this.login_id = login_id;
		}
		public String getBranch_name() {
			return branch_name;
		}
		public void setBranch_name(String branch_name) {
			this.branch_name = branch_name;
		}
		public String getPosition_name() {
			return position_name;
		}
		public void setPosition_name(String position_name) {
			this.position_name = position_name;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getIs_stopped() {
			return is_stopped;
		}
		public void setIs_stopped(int is_stopped) {
			this.is_stopped = is_stopped;
		}






}
