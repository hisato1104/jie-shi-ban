package billboard.contlorl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import billboard.Service.UserService;
import billboard.beans.User;

@WebServlet(urlPatterns = { "/user_alive" })
public class UserAliveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

//		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		new UserService().userAlive(editUser);

//		session.setAttribute("loginUser", editUser);

		response.sendRedirect("user_view");

	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		int changeCondition = Integer.parseInt(request.getParameter("is_stopped")) - 1;
		editUser.setIs_stopped(Math.abs(changeCondition));
		return editUser;
	}

}
