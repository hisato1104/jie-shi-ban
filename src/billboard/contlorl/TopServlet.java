package billboard.contlorl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import billboard.Service.CommentService;
import billboard.Service.MessageService;
import billboard.beans.Message;
import billboard.beans.User;
import billboard.beans.UserMessage;
import billboard.beans.UserMessageComment;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

	User user = (User) request.getSession().getAttribute("loginUser");
    boolean isShowMessageForm;
    if (user != null) {
        isShowMessageForm = true;
    } else {
        isShowMessageForm = false;
    }

    //jspから情報を取得
    String startman = request.getParameter("start");
    String endman = request.getParameter("end");
    String category = request.getParameter("catename");

    List<UserMessage> messages = new MessageService().getMessage(startman,endman,category );
    List<UserMessageComment> comments = new CommentService().getComment();


    request.setAttribute("messages", messages);
    request.setAttribute("comments", comments);
    request.setAttribute("isShowMessageForm", isShowMessageForm);

    request.getRequestDispatcher("/top.jsp").forward(request, response);
    }

protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {
//	String startman = request.getParameter("start");
//    String endman = request.getParameter("end");
//    String category = request.getParameter("catename");

	Message selectMessage = getMessages(request);


        request.setAttribute("messages", selectMessage);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

private Message getMessages(HttpServletRequest request)
        throws IOException, ServletException {

	Message selectMessage = new Message();
	try {
	SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
	selectMessage.setCreatedDate(sdFormat.parse((request.getParameter("start"))));
	selectMessage.setCreatedDate(sdFormat.parse((request.getParameter("end"))));
	selectMessage.setCategory(request.getParameter("catename"));
	 } catch (ParseException e) {
	 }


    return selectMessage;
}
}

