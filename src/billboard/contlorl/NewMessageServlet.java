package billboard.contlorl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import billboard.Service.MessageService;
import billboard.beans.Message;
import billboard.beans.User;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setCategory(request.getParameter("category"));
            message.setSubject(request.getParameter("subject"));
            message.setText(request.getParameter("message"));


            message.setUser_Id(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);

//            response.sendRedirect("./");
            request.getRequestDispatcher("top.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String message = request.getParameter("message");
        String subject = request.getParameter("subject");

        if (StringUtils.isEmpty(message) == true) {
            messages.add("メッセージを入力してください");
        }
        if (StringUtils.isEmpty(subject) == true) {
            messages.add("メッセージを入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
