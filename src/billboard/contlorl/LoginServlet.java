package billboard.contlorl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import billboard.Service.LoginService;
import billboard.beans.User;


@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String accountOrEmail = request.getParameter("login_id");
        String password = request.getParameter("password");
        String is_stopped = request.getParameter("is_stopped");


        LoginService loginService = new LoginService();
        User user = loginService.login(accountOrEmail, password, is_stopped);

        HttpSession session = request.getSession();

        if (user != null) {

            session.setAttribute("loginUser", user);
            response.sendRedirect("./");

        } else {

            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました。");
            messages.add("ログインID・パスワードを再入力してください。");
            if(is_stopped == "1" ) {
            	messages.add("当ログインIDは無効中です。");
            }
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
//            response.sendRedirect("login");
        }
    }
//    public class LoginCheckFilter implements Filter{
//        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{
//            System.out.println("ログインチェック");
//
//            // セッションが存在しない場合NULLを返す
//            HttpSession session = ((HttpServletRequest)req).getSession(false);
//
//            if(session != null){
//                // セッションがNULLでなければ、通常どおりの遷移
//                chain.doFilter(req, res);
//            }else{
//                // セッションがNullならば、ログイン画面へ飛ばす
//                RequestDispatcher dispatcher = req.getRequestDispatcher("/loginform");
//                dispatcher.forward(req,res);
//            }
//
//        }
//
//        public void init(FilterConfig config) throws ServletException{}
//        public void destroy(){}
//    }


}