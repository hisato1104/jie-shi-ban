package billboard.contlorl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import billboard.Service.CommentService;
import billboard.beans.Comment;
import billboard.beans.Message;
import billboard.beans.User;
import billboard.beans.UserMessageComment;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

            User user = (User) request.getSession().getAttribute("loginUser");

            Comment comment = new Comment();
            Message message = new Message();

            if (comment.getMessage_id() == message.getId()) {
            boolean isShowCommentForm;
            if (user != null) {
                isShowCommentForm = true;
            } else {
                isShowCommentForm = false;
            }

            List<UserMessageComment> comments = new CommentService().getComment();

            request.setAttribute("comments", comments);
            request.setAttribute("isShowCommentForm", isShowCommentForm);

            request.getRequestDispatcher("/top.jsp").forward(request, response);
            }
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> comments = new ArrayList<String>();



        if (isValid(request, comments) == true) {

            User user = (User) session.getAttribute("loginUser");
//            Message message = (Message) session.getAttribute("loginMessage");

            Comment comment = new Comment();

//
              comment.setText(request.getParameter("text"));
              comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));
              comment.setUser_Id(user.getId());
//

            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", comments);

            response.sendRedirect("./");
        }


    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String text = request.getParameter("text");

        if (StringUtils.isEmpty(text) == true || text.equals(" ") == true || text.equals("　") == true) {
            messages.add("コメントを入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}