package billboard.contlorl;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import billboard.Service.CommentService;
import billboard.beans.Comment;

@WebServlet(urlPatterns = { "/commentdelete" })
public class CommentDeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {





            Comment commentDelete = new Comment();

              commentDelete.setId(Integer.parseInt(request.getParameter("id")));

              try {


           	  new CommentService().commentDelete(commentDelete);

              response.sendRedirect("./");
              }catch(SQLException e ) {
            	  e.printStackTrace();
              }

    }



}
