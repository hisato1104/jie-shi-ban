package billboard.contlorl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import billboard.Service.UserService;
import billboard.beans.Branch;
import billboard.beans.Position;
import billboard.beans.User;
import billboard.exception.NoRowsUpdatedRuntimeException;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException,ServletException  {


        List<String> errorMessages = new ArrayList<String>();
        HttpSession session = request.getSession();
//    	 User user = (User) request.getSession().getAttribute("loginUser");

        int id = 0;

        try {
        	id = Integer.parseInt(request.getParameter("id"));
        	User userId = new UserService().getUserId(id);


        	if(userId == null) {
        	errorMessages.add("編集対象のユーザーが存在しません");
           	session.setAttribute("errorMessages", errorMessages);
           	response.sendRedirect("user_view");
           	return;
            }

        	session.setAttribute("user", userId);

        	List<Branch> branches = new UserService().getBranch();
            List<Position> positions = new UserService().getPosition();

//        	request.setAttribute("user", userId);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);

            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }catch(NumberFormatException e) {
        	errorMessages.add("編集対象のユーザーが存在しません");
          	 session.setAttribute("errorMessages", errorMessages);

            response.sendRedirect("user_view");
           	return;

        }


    }

    // ユーザ編集後の保持

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);
        List<Branch> branches = new UserService().getBranch();
        List<Position> positions = new UserService().getPosition();

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("/settings.jsp").forward(request, response);
                return;
            }

//            session.setAttribute("loginUser", editUser);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("user", editUser);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            request.getRequestDispatcher("/settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

    	User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {


    	String name = request.getParameter("name");
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String branch_id = request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");
//        User countUser = new UserService().getLoginid(login_id);


        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (name.length() < 11 != true) {
            messages.add("ユーザー名は10文字以下で入力してください");
        }
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
//        if(countUser != null) {
//            messages.add("ログインIDが重複してます");
//        }
        if (!login_id.matches(".*[0-9a-zA-Z]{6,20}.*")) {
            messages.add("ログインIDを6文字以上20文字以内で入力してください");
        }
        if (login_id.matches(".*[0-9a-zA-Z]{21,}.*")) {
            messages.add("ログインIDを6文字以上20文字以内で入力してください");
        }
        if(login_id.length() < 6){
          	 messages.add("ログインIDは6文字以上で入力してください");
     		}

        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }else if((request.getParameter("password")).matches(".*[0-9a-zA-Z]{21,}.*")){
         	 messages.add("パスワードは20文字以下で入力してください");
 		}else if(password.length() < 6){
 	       	 messages.add("パスワードは6文字以上で入力してください");
 	  		}

        if (branch_id == "1") {
        	if (!((position_id == "1") || (position_id == "2"))) {
        		messages.add("支店と部署・役職の組み合わせが不正です");
        	}
        }else {
        	if ((position_id == "1") || (position_id == "2")) {
        		messages.add("支店と部署・役職の組み合わせが不正です");
        	}
        }

//        if((request.getParameter("password")).matches(".*[0-9a-zA-Z]{21,}.*")){
//          	 messages.add("パスワードは20文字以下で入力してください");
//     		}
//        if(password.length() < 6){
//          	 messages.add("パスワードは6文字以上で入力してください");
//     		}
        if (StringUtils.isEmpty(branch_id) == true) {
            messages.add("店名を入力してください");
        }
        if (StringUtils.isEmpty(position_id) == true) {
            messages.add("役職名を入力してください");
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}