package billboard.contlorl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import billboard.Service.UserService;
import billboard.beans.Branch;
import billboard.beans.Position;
import billboard.beans.User;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branches = new UserService().getBranch();
        List<Position> positions = new UserService().getPosition();

        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);

        request.getRequestDispatcher("/signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	User newUser = getNewUser(request);
        List<String> messages = new ArrayList<String>();
        List<Branch> branches = new UserService().getBranch();
        List<Position> positions = new UserService().getPosition();


        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

//response
            new UserService().register(user);

            response.sendRedirect("user_view");
        } else {

        	session.setAttribute("errorMessages", messages);
            request.setAttribute("users", newUser);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);

            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private User getNewUser(HttpServletRequest request)
            throws IOException, ServletException {

    	User newUser = new User();
        newUser.setLogin_id(request.getParameter("login_id"));
        newUser.setPassword(request.getParameter("password"));
        newUser.setName(request.getParameter("name"));
        newUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        newUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

        return newUser;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String branch_id = request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");
        User countUser = new UserService().getLoginid(login_id);


        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (name.length() > 11 ) {
            messages.add("ユーザー名は10文字以下で入力してください");
        }

        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }

        if(countUser != null) {
            messages.add("ログインIDが重複してます");
        }

        if((request.getParameter("login_id")).matches(".*[0-9a-zA-Z]{21,}.*")){
        	 messages.add("ログインIDは20文字以下で入力してください");
   		}


        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }else if((request.getParameter("password")).matches(".*[0-9a-zA-Z]{21,}.*")){
          	 messages.add("パスワードは20文字以下で入力してください");
  		}else if(password.length() < 6){
  	       	 messages.add("パスワードは6文字以上で入力してください");
  	  		}



        if (StringUtils.isEmpty(branch_id) == true) {
            messages.add("所属店を入力してください");
        }

        if (StringUtils.isEmpty(position_id) == true) {
            messages.add("役職を入力してください");
        }

        if ((request.getParameter("branch_id")).matches ("1")) {
        	if (!((request.getParameter("position_id").matches ("1")) || (request.getParameter("position_id").matches ("2")))) {
        		messages.add("支店と部署・役職の組み合わせが不正です");
        	}
        }else {
        	if ((request.getParameter("position_id").matches ("1")) || request.getParameter("position_id").matches ("2")){
        		messages.add("支店と部署・役職の組み合わせが不正です");
        	}
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}