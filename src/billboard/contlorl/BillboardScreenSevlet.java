package billboard.contlorl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import billboard.Service.MessageService;
import billboard.beans.Message;
import billboard.beans.User;
import billboard.beans.UserMessage;

@WebServlet(urlPatterns = { "/billboard-screen" })
	public class BillboardScreenSevlet extends HttpServlet {
    	private static final long serialVersionUID = 1L;
@Override

//
//
   	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

            User user = (User) request.getSession().getAttribute("loginUser");
            boolean isShowMessageForm;
            if (user != null) {
                isShowMessageForm = true;
            } else {
                isShowMessageForm = false;
            }


            String startman = request.getParameter("start");
            String endman = request.getParameter("end");
            String category = request.getParameter("catename");

            List<UserMessage> messages = new MessageService().getMessage(startman, endman, category);

            request.setAttribute("messages", messages);
            request.setAttribute("isShowMessageForm", isShowMessageForm);

			request.getRequestDispatcher("Billboard.jsp").forward(request, response);
			}
	protected void doPost(HttpServletRequest request,
	        HttpServletResponse response) throws IOException, ServletException {

	    HttpSession session = request.getSession();

	    List<String> messages = new ArrayList<String>();

	    if (isValid(request, messages) == true) {

	        User user = (User) session.getAttribute("loginUser");

	        Message message = new Message();
	        message.setCategory(request.getParameter("category"));
	        message.setSubject(request.getParameter("subject"));
	        message.setText(request.getParameter("message"));
	        message.setUser_Id(user.getId());

	        new MessageService().register(message);

	        response.sendRedirect("./");
	    } else {
	        session.setAttribute("errorMessages", messages);
//	        request.getRequestDispatcher("top.jsp").forward(request, response);
	        response.sendRedirect("billboard-screen");
	    }
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String category = request.getParameter("category");
	    String subject = request.getParameter("subject");
	    String message = request.getParameter("message");

	    if (StringUtils.isEmpty(category) == true || category.equals(" ") == true || category.equals("　") == true || StringUtils.isBlank(category) == true ) {
	        messages.add("カテゴリーを入力してください");
	    }
	    if (StringUtils.isEmpty(subject) == true || subject.equals(" ") == true || subject.equals("　") == true || StringUtils.isBlank(subject) == true ) {
	        messages.add("件名を入力してください");
	    }

	    if (StringUtils.isEmpty(message) == true || message.equals(" ") == true || message.equals("　") == true || StringUtils.isBlank(message) == true) {
	        messages.add("本文を入力してください");
	    }
	    if (messages.size() == 0) {
	        return true;
	    } else {
	        return false;
	    }
	}

	}
