package billboard.contlorl;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import billboard.Service.MessageService;
import billboard.beans.Message;

@WebServlet(urlPatterns = { "/messagedelete" })
public class MessageDeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

			Message messageDelete = new Message();

			messageDelete.setId(Integer.parseInt(request.getParameter("id")));


              try {


           	  new MessageService().MessageDelete(messageDelete);

              response.sendRedirect("./");
              }catch(SQLException e ) {
            	  e.printStackTrace();
              }

    }



}
