package billboard.contlorl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import billboard.Service.UserManagementService;
import billboard.beans.User;
import billboard.beans.UserManagement;

@WebServlet(urlPatterns = { "/user_view" })
	public class UserManagementServlet extends HttpServlet {
    	private static final long serialVersionUID = 1L;
@Override

   	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



            User user = (User) request.getSession().getAttribute("loginUser");
            boolean isShowMessageForm;
            if (user != null) {
                isShowMessageForm = true;
            } else {
                isShowMessageForm = false;
            }

            List<UserManagement> members = new UserManagementService().getUserManagement();
//            List<String> messages = new ArrayList<String>();
//            HttpSession session = request.getSession();

            request.setAttribute("users", members);
//            session.setAttribute("errorMessages", messages);
            request.setAttribute("isShowMessageForm", isShowMessageForm);

			request.getRequestDispatcher("UserManagement.jsp").forward(request, response);
			}

}
