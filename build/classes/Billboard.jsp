<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <link href="./css/save.css" rel="stylesheet" type="text/css">
        <title>みんなの掲示板</title>
</head>
<body>


	<script type="text/javascript">
		function submitbtn() {
			  return confirm('実行しますか？');
		}
		</script>

		<div class="errorMessages">
		 <c:forEach items="${errorMessages}" var="message">
        <c:if test="${ not empty errorMessages }">
            <ul>
              <li><c:out value="${message}" />
            </ul>
        <c:remove var="errorMessages" scope="session"/>
        </c:if>
      </c:forEach>
		</div>

<div class="form-area">
        <form action="billboard-screen" method="post" onSubmit="return submitbtn();" >
            <div class="text">
            <div class="title">
            掲示板投稿<br />
			</div>
            <br/>

            <br/>
            <label for="category" >カテゴリー</label><br/>
			<input name="category" id="category"/>
			<br/>

            <br/>
            <label for="subject">件名</label><br/>
            <input name="subject" id="subject"/>
			<br/>
			内容
			<br/>
       		<textarea name="message" cols="100" rows="5" class="tweet-box"  maxlength='1000'></textarea>

            <br />
            <input type="submit" value="投稿">
            <br />
            <a href="./index.jsp">戻る</a>
            </div>
        </form>
		</div>



<br/>
<br/>
<br/>
<br/>
<div class="messa"> What do you want to post?</div>

</body>
</html>