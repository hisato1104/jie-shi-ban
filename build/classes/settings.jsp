<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<link href="./css/save.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${users.name}の設定</title>

    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>



            <form action="settings" method="post"><br />
                <input name="id" value="${user.id}" id="id" type="hidden"/>

                <label for="name">名前</label><br />
                <input name="name" value="${user.name}" id="name"/>（名前はあなたの公開プロフィールに表示されます）<br />

                <label for="login_id">ログインID</label><br />
                <input name="login_id" value="${user.login_id}" /><br />

                <label for="password">パスワード</label><br />
                <input name="password" type="password" id="password"/> <br />

                <label for="password">確認用パスワード</label><br />
                <input name="password" type="password" id="password"/> <br />

				<c:if test="${user.id != loginUser.id}">

				支店名<br>
				<select name="branch_id">

				<c:forEach items="${branches}" var="branch">
					<c:if test="${branch.id == user.branch_id}">
						<option value="${branch.id}" selected>${branch.name}</option>

					</c:if>
					<c:if test="${branch.id != user.branch_id }">
						<option value="${branch.id}">${branch.name}</option>
					</c:if>

				</c:forEach>
			</select>
				<br />

				役職名<br>
				<select name="position_id">
				<c:forEach items="${positions}" var="position">
					<c:if test="${position.id == user.position_id }">
						<option value="${position.id}" selected>${position.name}</option>
					</c:if>
					<c:if test="${position.id != user.position_id }">
						<option value="${position.id}">${position.name}</option>
					</c:if>
				</c:forEach>
			</select> <br />
				</c:if>

				<c:if test="${user.id == loginUser.id}">
				<input name="branch_id" value="${loginUser.branch_id}" type="hidden"/>
				<input name="position_id" value="${loginUser.position_id}" type="hidden"/>
				</c:if>
				<br />
                <input type="submit" value="登録" /> <br />
                <a href="./user_view">戻る</a>
            </form>

        </div>
        <br /><br />
        <br />
        <div class="messa"><br />Please edit User information</div>
    </body>
</html>