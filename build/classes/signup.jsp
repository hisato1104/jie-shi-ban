<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/save.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">

			<br /> <label for="name">名前</label><br /> <input name="name"
				value="${users.name}" id="name" />（名前はあなたの公開プロフィールに表示されます）<br /> <label
				for="login_id">ログインID</label><br /> <input name="login_id"
				value="${users.login_id}" id="login_id" /> <br /> <label
				for="password">パスワード</label> <br /><input name="password" type="password"
				id="password" /> <br /> <label for="password">確認用パスワード</label> <br /><input
				name="password" type="password" id="password" /> <br />

			支店名<br>
			<select name="branch_id">
				<c:forEach items="${branches}" var="branch">
					<c:if test="${branch.id == users.branch_id }">
						<option value="${branch.id}" selected>${branch.name}</option>
					</c:if>
					<c:if test="${branch.id != users.branch_id }">
						<option value="${branch.id}">${branch.name}</option>
					</c:if>
				</c:forEach>
			</select>
			<br />
			役職名<br>
			 <select name="position_id">
				<c:forEach items="${positions}" var="position">
					<c:if test="${position.id == users.position_id }">
						<option value="${position.id}" selected>${position.name}</option>
					</c:if>
					<c:if test="${position.id != users.position_id }">
						<option value="${position.id}">${position.name}</option>
					</c:if>
				</c:forEach>
			</select> <br /> <br />
			<input type="submit" value="登録" /> <br /> <a href="user_view">戻る</a>
		</form>

	</div>
</body>
</html>