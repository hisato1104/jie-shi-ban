<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page import="java.util.*"%>
<!DOCTYPE html>

<html>
<head><link href="./css/top.css" rel="stylesheet" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>掲示板</title>
</head>

<body>
<div class="pagebody">
<div class ="header"><h1>サッカー選手伝説</h1></div>
<div class ="logname">こんにちわ！<c:out value="${loginUser.name }" />さん！<br />
<c:if test="${ not empty loginUser }">
			<a href="./">ホーム</a>
			<a href="logout">ログアウト</a>
			</c:if>
			</div>

<div class="errorMessages">
		<c:forEach items="${errorMessages}" var="message">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<li><c:out value="${message}" />
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</c:forEach>
	</div>

	<script type="text/javascript">
		function submitbtn() {
			  return confirm('実行しますか？');
		}
	</script>
	<div class ="subbody">
		<div class ="submenu_body">
		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
			<a href="signup">登録する</a>
		</c:if>

		<br />
		<a href="billboard-screen">掲示板投稿画面</a>
		<c:if test="${loginUser.position_id == 1 || loginUser.branch_id == 1}">
		<a href="user_view">ユーザ一覧</a>
		</c:if>
		<br />

		<form action="./">
			<label>開始日：<input type="date" name="start" ></label><br />
			<label>終了日：<input type="date" name="end" ></label>
			<br />
			 <label>カテゴリー：<input type="text" name="catename" ></label>
			<input type="submit" value="絞り込み">
		</form>
		</div>

		<div class ="billboard">
		<c:forEach items="${messages}" var="message">
			<div class="messages">
				<div class="message">

						<br /> <br />
						<br />
					<div class="billboardname">
						投稿者：<span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="billboardarea">
					<br /> [カテゴリー ]
					<div class="category">
						<c:out value="${message.category}" />
					</div><br />
					[件名]
					<div class="subject">
						<c:out value="${message.subject}" />
					</div><br />
					[投稿内容]

					<c:forEach  items="${fn:split(message.text, '
')}" var="s" >
						<div>
						<c:out value = "${s}"/>
						</div>
						</c:forEach>

					<br />
					<div class="date">
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />


					<form action="messagedelete" method="post"
						onSubmit="return submitbtn();">
						<c:if test="${loginUser.name == message.name}">
							<input type="submit" value="削除">
							<input type="hidden" name="id" value="${message.id}" />
						</c:if>
					</form>
				</div>
				</div>
<div class="commentview">
        ---------------------☆コメント一覧☆---------------------
        <c:forEach items="${comments}" var="comment">
				<div class="comments">
					<div class="comment">
						<div class="account-name">
							<c:if test="${message.id == comment.message_id}">

								<div class="box-title">
								<span class="name"><c:out value="${comment.name}" /></span>
								</div>
								<div class="text">
								<c:forEach  items="${fn:split(comment.text, '
')}" var="s" >
								<c:out value = "${s}"/>
								</c:forEach>
								</div>
								<div class="date">
									<fmt:formatDate value="${comment.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />


								<form action="commentdelete" method="post"
									onSubmit="return submitbtn();">
									<c:if test="${loginUser.name == comment.name}">
										<input type="submit" value="削除" />
										<input type="hidden" name="id" value="${comment.id}" />
									</c:if>
								</form>
								</div>
							</c:if>
						</div>
					</div>
				</div>

			</c:forEach>
			</div>
			</div>
			<div class="form-area">
				<form action="comment" method="post" onSubmit="return submitbtn();">
					<br /> コメント入力欄 <br />
					<input type="hidden" name="message_id" value="${ message.id }" />
					<input type="hidden" name="id" value="${comment.id}" />
					<div class="commentarea">
					<textarea name="text" cols="50" rows="7" class="tweet-box" maxlength='500'></textarea>
					</div>
					<br />
					<input type="submit" value="コメントGO!!">
				</form>
			</div>
			</div>
		</c:forEach>
	</div>
	</div>
	</div>

</body>
</html>