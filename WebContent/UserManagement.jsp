<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/Management.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザ一覧</title>
</head>
<body>


		<script type="text/javascript">
		function submitbtn() {
			  return confirm('実行しますか？');

		}

		</script>



	<div class="header">
		<div class="title">
		★ユーザ一覧★
		</div>
		<br />
		<a href="signup">新規登録</a>
		<a href="./">戻る</a>
		<br />

		<div class="errorMessages">
			<c:if test="${ not empty errorMessages }">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
	</div>

		<c:forEach items="${users}" var="user">



			<div class="users">

				<div class="user">
				<br />

					<div class="inline-block">
					<span class="name"><c:out value="${user.name}" /></span>
					</div><div class="inline-block"><c:out value="${user.login_id}" />
					</div><div class="inline-block">
						<c:out value="${user.branch_name}" />
					</div><div class="inline-block"><c:out value="${user.position_name}" />
					</div><div class="inline-block">
					<form action="settings" method="get"><input name="id" value="${user.id}" type="hidden"/><input type="submit" value=編集する  /> <br />
					</form>
					</div><c:if test="${loginUser.id != user.id}"><c:if test="${user.is_stopped == 1}"><div class="inline-block">
							<form action="user_alive" method="post" onSubmit="return submitbtn();">
								<input name="id" value="${user.id}" id="id" type="hidden"/>
								<input name="is_stopped" value="${user.is_stopped}" id="id" type="hidden"/>
								<input type="submit" value=無効中  /> <br />
							</form>
							</div></c:if><c:if test="${user.is_stopped == 0}"><div class="inline-block">
							<form action="user_alive" method="post"onSubmit="return submitbtn();" >
							<input name="id" value="${user.id}" id="id" type="hidden"/>
							<input name="is_stopped" value="${user.is_stopped}" id="id" type="hidden"/>
							<input type="submit" value=有効中 /> <br />
							</form>
							</div>
						</c:if>

						</c:if>

					</div>
			</div>
		</c:forEach>

<a href="./">戻る</a>
</body>
</html>