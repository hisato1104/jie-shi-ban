<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>ログイン</title>
    </head>
    <body>

			<div class="errorMessages">
            <c:if test="${ not empty errorMessages }">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>

                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            </div>
			<div class="main-contents">
            <form action="login" method="post"><br />
            	<div class="heading">
				<h1 class="major">Login Form </h1>
				</div>

				<div class="sign-up">


                <label for="login_id">Login ID</label><br />
                <input name="login_id"   placeholder ="What's your Login Id?" id="login_id" /> <br />

                <label for="password">Password</label><br />
                <input name="password" type="password" placeholder ="What's your Password?"  id="password" /> <br />

                <input name="is_stopped" value="${loginUser.is_stopped}" id="is_stopped" type="hidden"/>
                <br />
                <br />
             	</div>

             	<div class="back">
                <input type="submit" value="Sign me up!" /> <br />
             	</div>
             </form>



                </div>

    </body>
</html>